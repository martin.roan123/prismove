import prisma from '../../src/clients/prisma';
import generateScheduleId from '../../src/utils/generateScheduleId';

const days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
const hours = '06.07.08.09.10.11.12.13.14.15.16.17.18.19.20.21.22.23'.split('.');
// const hours = '06.07'.split('.');
const minutes = ['00', '30'];

const times = hours.reduce((acc, hour) => {
  acc.push(`${hour}${minutes[0]}`);
  acc.push(`${hour}${minutes[1]}`);
  return acc;
}, []);

const schedules = [];

hours.forEach((beginHour) => {
  minutes.forEach((beginMinute) => {
    const beginTime = `${beginHour}${beginMinute}`;
    const endTimes = times.filter(endTime => parseInt(endTime, 10) > parseInt(beginTime, 10));
    endTimes.forEach((endTime) => {
      days.forEach((day) => {
        const begin = [beginTime.slice(0, 2), ':', beginTime.slice(2)].join('');
        const end = [endTime.slice(0, 2), ':', endTime.slice(2)].join('');
        schedules.push(() => prisma.createSchedule({
          customId: generateScheduleId(day, begin, end),
          day,
          begin,
          end,
        }));
      });
    });
  });
});

const chunk = (list, chunkSize) => {
  if (!list || (list && !list.length)) {
    return [];
  }

  let i;
  let j;
  let t;
  const chunks = [];
  for (i = 0, j = list.length; i < j; i += chunkSize) {
    t = list.slice(i, i + chunkSize);
    chunks.push(t);
  }

  return chunks;
};

const chunkedSchedules = chunk(schedules, 50);

const insertSchedules = async () => {
  for (let i = 0; i < chunkedSchedules.length; i += 1) {
    await Promise.all(chunkedSchedules[i].map(fn => fn()));
  }
  return Promise.resolve();
};

Promise.all([
  insertSchedules(),
])
  .then(() => {
    console.log('Time update finished');
  });
