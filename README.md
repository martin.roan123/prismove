# prismove

A simple way to move your data from a prisma 1 database to another

# How to init ?
- update dependencies : *yarn*
- create a .env (like the .env.example one)
- copy/paste your prisma folder with datamodel and prisma.yml if it have change
- run postgres and prisma dockers : *docker-compose up -d*
- generate and deploy prisma : *prisma generate; prisma deploy*

# How to run ?
- yarn prismove