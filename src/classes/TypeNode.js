import 'dotenv/config';
import chunk from 'lodash.chunk';

import newPrisma from '../clients/prisma';
import WARNINGPrismaProd from '../clients/prismaProd';
import knex from '../clients/knex';

const log = (message) => {
  if (process.env.ACTIVATE_LOGS) {
    console.log(message);
  }
};

const camelize = (str) => str.replace(
  /(?:^\w|[A-Z]|\b\w)/g,
  (word, index) => (index === 0 ? word.toLowerCase() : word.toUpperCase()),
).replace(/\s+/g, '');

const toPlurial = (str) => {
  switch (str.charAt(str.length - 1)) {
    case 's':
      return camelize(`${str}es`);
    case 'y':
      return camelize(`${str.slice(0, -1)}ies`);
    default:
      return camelize(`${str}s`);
  }
};

const recChunkedMap = async ([start, ...other], fn) => Promise.all(start.map(fn)).then(
  async (result) => {
    if (other.length) {
      return [...result, ...(await recChunkedMap(other, fn))];
    }
    return (result);
  },
);

const chunkedMap = async (array, fn, size = 25) => {
  if (!array.length) {
    return [];
  }

  const chunkedArray = chunk(array, size);

  return (await recChunkedMap(chunkedArray, fn));
};

export default class TypeNode {
  constructor(typeName, toCompute = true) {
    this.typeName = typeName;
    this.relations = [];
    this.isComputed = !toCompute;
    this.isComputing = false;
    this.result = [];
  }

  getName() {
    return this.typeName;
  }

  addRelation(typeNode, relationName, relationType = 'connect') {
    if (!relationName) {
      throw new Error(`You have to give a name for the relation of ${this.typeName}`);
    }
    this.relations.push({ relationName, relationType, typeNode });
  }

  isComputed() {
    return this.isComputed;
  }

  async getInsertContent(prisma) {
    return chunkedMap(
      await prisma[toPlurial(this.typeName)](),
      async ({ id, ...other }) => {
        try {
          const result = { id, ...other };

          await Promise.all(this.relations.map(async ({ relationName, relationType }) => {
            const relationData = await prisma[camelize(this.typeName)]({ id })[relationName]()
              .catch((err) => console.error('relation error', err));
            switch (relationType) {
              case 'connect':
                if (relationData && relationData !== null) {
                  result[relationName] = { connect: { id: relationData.id } };
                }
                break;
              case 'multipleConnect':
                if (relationData && relationData !== null) {
                  result[relationName] = { connect: relationData.map((d) => ({ id: d.id })) };
                }
                break;
              default:
                throw new Error(`relationType ${relationType} is unknown.`);
            }
          }));

          return result;
        } catch (err) {
          console.error(`unexpected error occured during reading for ${this.typeName} : ${err}.`);
          return {};
        }
      },
    );
  }

  async insert(prisma, data) {
    return chunkedMap(
      data,
      async ({ updatedAt, createdAt, ...other }) => {
        const res = await prisma[`create${this.typeName}`](other)
          .catch((err) => err.result);

        const knexRes = updatedAt && createdAt
          ? await knex(`default$default.${this.typeName}`)
            .where({ id: other.id })
            .update({
              createdAt,
              updatedAt,
            }, ['id', 'createdAt', 'updatedAt'])
            .catch(console.error)
          : null;

        if (!res?.errors || res.errors.length === 0) {
          return { data: res, errors: [], knex: knexRes };
        }
        return res;
      },
      100,
    );
  }

  async compute() {
    if (!this.isComputing && !this.isComputed) {
      this.isComputing = true;

      const result = await Promise.all(
        this.relations.map(({ typeNode }) => typeNode.compute()),
      );

      log(`retrieve data for ${this.typeName}...`);
      const toInsert = await this.getInsertContent(WARNINGPrismaProd);
      log(`insert ${toInsert.length} ${this.typeName} data...`);
      const data = await Promise.all(await this.insert(newPrisma, toInsert));

      log(`${this.typeName} have been computed !`);
      result.push({ typeName: this.typeName, data });
      this.isComputing = false;
      this.isComputed = true;
      this.result = result;
    }

    while (this.isComputing) {
      log(`waiting ${this.typeName}...`);
      await new Promise(r => setTimeout(r, 3000));
    }

    return this.result;
  }
}
