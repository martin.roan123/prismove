import 'dotenv/config';

import TypeNode from './classes/TypeNode';

const log = (message) => {
  if (process.env.ACTIVATE_LOGS) {
    console.log(message);
  }
};

const createGraph = () => {
  const AddressNode = new TypeNode('Address');
  const AdminNode = new TypeNode('Admin');
  const ContactEmailNode = new TypeNode('ContactEmail');
  const FileNode = new TypeNode('File');
  const PlanNode = new TypeNode('Plan');
  const SettingNode = new TypeNode('Setting');
  const QuestionTemplateNode = new TypeNode('QuestionTemplate');

  const UserNode = new TypeNode('User');
  UserNode.addRelation(AddressNode, 'addresses', 'multipleConnect');
  UserNode.addRelation(AddressNode, 'addressesOld', 'multipleConnect');
  UserNode.addRelation(FileNode, 'picture');

  const SellerNode = new TypeNode('Seller');
  SellerNode.addRelation(FileNode, 'logo');
  SellerNode.addRelation(FileNode, 'coverPictures', 'multipleConnect');
  SellerNode.addRelation(AddressNode, 'address');

  const AnswerTemplateNode = new TypeNode('AnswerTemplate');
  AnswerTemplateNode.addRelation(QuestionTemplateNode, 'question');
  AnswerTemplateNode.addRelation(QuestionTemplateNode, 'nextQuestion');

  const ScheduleNode = new TypeNode('Schedule');
  ScheduleNode.addRelation(SellerNode, 'sellers', 'multipleConnect');

  const WalletNode = new TypeNode('Wallet');
  WalletNode.addRelation(UserNode, 'user');

  const WalletTransactionNode = new TypeNode('WalletTransaction');
  WalletTransactionNode.addRelation(WalletNode, 'wallet');

  const CartNode = new TypeNode('Cart');
  CartNode.addRelation(UserNode, 'user');
  CartNode.addRelation(WalletTransactionNode, 'walletTransactionIn');
  CartNode.addRelation(WalletTransactionNode, 'walletTransactionOut');

  const ItemNode = new TypeNode('Item');
  ItemNode.addRelation(SellerNode, 'seller');
  ItemNode.addRelation(FileNode, 'pinedPicture');
  ItemNode.addRelation(FileNode, 'pictures', 'multipleConnect');

  const CartItemNode = new TypeNode('CartItem');
  CartItemNode.addRelation(SellerNode, 'seller');
  CartItemNode.addRelation(ItemNode, 'item');
  CartItemNode.addRelation(CartNode, 'cart');

  const ItemCategoryNode = new TypeNode('ItemCategory');
  ItemCategoryNode.addRelation(SellerNode, 'sellers', 'multipleConnect');
  ItemCategoryNode.addRelation(QuestionTemplateNode, 'firstQuestion');
  ItemCategoryNode.addRelation(QuestionTemplateNode, 'lastQuestions', 'multipleConnect');
  ItemCategoryNode.addRelation(QuestionTemplateNode, 'questions', 'multipleConnect');
  ItemCategoryNode.addRelation(ItemNode, 'items', 'multipleConnect');

  const ItemCategoryRelationNode = new TypeNode('ItemCategoryRelation');
  ItemCategoryRelationNode.addRelation(ItemCategoryNode, 'parentCategory');
  ItemCategoryRelationNode.addRelation(ItemCategoryNode, 'nextCategories', 'multipleConnect');

  const UbimItemNode = new TypeNode('UbimItem');
  UbimItemNode.addRelation(UserNode, 'user');
  UbimItemNode.addRelation(SellerNode, 'seller');
  UbimItemNode.addRelation(WalletTransactionNode, 'walletTransactionIn');
  UbimItemNode.addRelation(WalletTransactionNode, 'walletTransactionOut');

  const ShopTypeNode = new TypeNode('ShopType');
  ShopTypeNode.addRelation(ItemCategoryNode, 'recommendedItemCategories', 'multipleConnect');
  ShopTypeNode.addRelation(SellerNode, 'sellers', 'multipleConnect');
  ShopTypeNode.addRelation(SellerNode, 'primarySellers', 'multipleConnect');
  ShopTypeNode.addRelation(UserNode, 'users', 'multipleConnect');

  const FriendNode = new TypeNode('Friend');
  FriendNode.addRelation(AddressNode, 'address');
  FriendNode.addRelation(UserNode, 'friendOf');
  FriendNode.addRelation(UserNode, 'friendOfOld');
  FriendNode.addRelation(FileNode, 'picture');
  FriendNode.addRelation(ShopTypeNode, 'preferedShopTypes', 'multipleConnect');

  const GiftNode = new TypeNode('Gift');
  GiftNode.addRelation(ItemNode, 'item');
  GiftNode.addRelation(UbimItemNode, 'ubimItem');
  GiftNode.addRelation(UserNode, 'user');
  GiftNode.addRelation(UserNode, 'receivers', 'multipleConnect');
  GiftNode.addRelation(FriendNode, 'friend');
  GiftNode.addRelation(FileNode, 'picture');
  GiftNode.addRelation(SellerNode, 'seller');
  GiftNode.addRelation(WalletTransactionNode, 'walletTransactionIn');
  GiftNode.addRelation(WalletTransactionNode, 'walletTransactionOut');

  const SellerPaymentNode = new TypeNode('SellerPayment');
  SellerPaymentNode.addRelation(SellerNode, 'seller');
  SellerPaymentNode.addRelation(ItemNode, 'items', 'multipleConnect');
  SellerPaymentNode.addRelation(CartItemNode, 'cartItems', 'multipleConnect');
  SellerPaymentNode.addRelation(GiftNode, 'gifts', 'multipleConnect');
  SellerPaymentNode.addRelation(UbimItemNode, 'ubimItems', 'multipleConnect');

  const SellerSubscriptionNode = new TypeNode('SellerSubscription');
  SellerSubscriptionNode.addRelation(SellerNode, 'seller');
  SellerSubscriptionNode.addRelation(PlanNode, 'plan');

  return [SellerSubscriptionNode, SellerPaymentNode, ItemCategoryRelationNode,
    AdminNode, ContactEmailNode, AnswerTemplateNode, SettingNode, ScheduleNode];
};

const main = async () => {
  const nodes = createGraph();

  const result = (await Promise.all(nodes.map((typeNode) => typeNode.compute()))).flat(1);

  const recap = (typeName, data) => data.reduce(
    (acc, elem) => {
      if (elem.errors.length <= 0) {
        return { ...acc, success: [...acc.success, elem] };
      }
      if (
        elem.errors.length === 1
        && elem.errors[0].code === 3010
        && elem.errors[0].message.includes(typeName)
      ) {
        return { ...acc, alreadyInserted: [...acc.alreadyInserted, elem] };
      }
      return { ...acc, errors: [...acc.errors, elem] };
    },
    { success: [], alreadyInserted: [], errors: [] },
  );

  const resultSlice = result.find((res) => res?.typeName === nodes[0].getName());
  const logs = recap(resultSlice.typeName, resultSlice.data);

  log(logs.errors.map(({errors}) => errors));
  log(`success: ${logs.success.length}, already inserted: ${logs.alreadyInserted.length}, errors: ${logs.errors.length}`);
  process.exit(0);
};

main();
