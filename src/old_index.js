import 'dotenv/config';
import knex from 'knex';

import prisma from './clients/prisma';

import node1 from '../toInsert/nodes/000001.json';
import node2 from '../toInsert/nodes/000002.json';
import node3 from '../toInsert/nodes/000003.json';

import relation1 from '../toInsert/relations/000001.json';
import relation2 from '../toInsert/relations/000002.json';
import relation3 from '../toInsert/relations/000003.json';
import relation4 from '../toInsert/relations/000004.json';
import relation5 from '../toInsert/relations/000005.json';
import relation6 from '../toInsert/relations/000006.json';
import relation7 from '../toInsert/relations/000007.json';

const query = (typeName, create, update=undefined) => {
  if (update) {
    return prisma[`upsert${typeName}`]({where: {id: create.id}, create, update});
  }
  return prisma[`create${typeName}`](create);
}

const getTypeNames = (values) => {
  const result = new Set();

  // values.forEach(
  //   ({_typeName}) => result.add(_typeName)
  // );

  // result.add('CartItem');
  result.add('Address');
  result.add('Admin');
  result.add('AnswerTemplate');
  result.add('Cart');
  result.add('CartItem');
  result.add('ContactEmail');
  result.add('File');
  result.add('ItemCategoryRelation');
  result.add('Plan');
  result.add('QuestionTemplate');
  result.add('Reset');
  result.add('Schedule');
  result.add('SellerPayment');
  result.add('ShopType');
  result.add('UbimItem');
  return [...result];
}

const generateRelationNode = (node, relations, alreadyExist=[]) => {
  // console.log("before", node);
  const result = {
    ...node,
    ...Object.entries(relations).reduce((acc, [key, value]) => {
      // console.log(key);
      return {
        ...acc,
        [key]: alreadyExist.includes(value.id) ?
          {'connect': {id: value.id}} :
          {'create': value}
      }}, {}),
  };
  // console.log('after', result);
  return result;
}

const formatUpdate = ({id, ...node}) => node;

const main = async () => {
  // const typeNames = ['Admin'];

  const values = [
    ...node1.values,
    ...node2.values,
    ...node3.values,
  ];

  const relations = [
    ...relation1.values,
    ...relation2.values,
    ...relation3.values,
    ...relation4.values,
    ...relation5.values,
    ...relation6.values,
    ...relation7.values,
  ];

  const typeNames = getTypeNames(values);

  // init nodes
  console.log("init nodes for those type names : ", typeNames);
  const nodes = typeNames
    .reduce(
      (acc, currentTypeName) => (
        {
          ...acc,
          [currentTypeName]: values.filter(
            ({ _typeName }) => _typeName === currentTypeName
          ).reduce(
            (accbis, {_typeName, id, updatedAt, createdAt, ...other }) => ({
              data: [...accbis.data, {id, ...other}],
              imutable: [...accbis.imutable, {id, updatedAt, createdAt}]
            }), {data: [], imutable: []}
          )
        }
      ), {}
    )

  console.log("create relations...");
  typeNames.forEach(
    (typeName) => relations
      .filter(
        ([
          { _typeName: t1, fieldName: f1 },
          { _typeName: t2, fieldName: f2 }
        ]) => (typeName === t1 && f1) || (typeName === t2 && f2)
      )
      .forEach(
        ([r1, r2]) => {
          const [closeRelationSide, remoteRelationSide] = typeName === r1._typeName ? [r1, r2] : [r2, r1];
          const searchInNodes = (tp) => {
            // console.log(tp);
            const res = nodes[tp].data.find(({id}) => id === closeRelationSide.id);
            // console.log("result", res);
            return res;
          }

          console.log("closeRelationSide", closeRelationSide);
          console.log("remoteRelationSide", remoteRelationSide);
          console.log("should be found", values.find(({id}) => id === closeRelationSide.id));

          const {_typeName, updatedAt, createdAt, ...remoteObject} = values.find(({id}) => id === remoteRelationSide.id);

          const node = searchInNodes(typeName); // || searchInNodes(values.find(({id}) => id === closeRelationSide.id)._typename);
          
          if (node) {
            node._relations = { ...(node._relations || {}), [closeRelationSide.fieldName]: remoteObject };
          } else {
            console.log(`WARNING : this relation doesn't exist :\n`
            + `closeRelationSide : ${closeRelationSide}\n`
            + `remoteRelationSide : ${remoteRelationSide}\n`)
            // + `because : ${values.find(({id}) => id === closeRelationSide.id}\n`)
          }
          console.log(node);
          // console.log(node);
        }
      )
  )

  console.log("insert everything inside prisma...")

  // add relations
  // const leftRelationNodes = relations.map 



  // console.log("NODES", nodes['AnswerTemplate'].data.find(({ id }) => id === 'cki7not2l8ro00746rgq62muf'));

  // console.log(nodes.AnswerTemplate);
  // console.log(nodes.AnswerTemplate.data.length);

  const result = (await Promise.all(Object.entries(nodes).map(
    async ([typeName, {data}]) => await Promise.all(
      data.map(async ({_relations, ...node}) => query(
        typeName,
        generateRelationNode(node, _relations || []),
        // formatUpdate(generateRelationNode(node, _relations || [])),
      )
        // .then((res) => {console.log("res", res); return { inserted: true, result: res }})
        .catch(
          (err1) => {
            return query(
              typeName,
              generateRelationNode(node, _relations || [], Object.values(_relations || []).map(({id}) => id)),
              // formatUpdate(generateRelationNode(node, _relations || [], Object.values(_relations || []).map(({id}) => id))),
            ).catch((err2) => ([err1.result.errors, err2.result.errors]) )
          }
        )
      )
    )
  ))).flat(1);
 
  result.slice(0, 1).forEach(console.log);

  console.log('RESULT', result.reduce(
    (acc, r) => (r?.errors) ? 
      ((r.errors.length && r.errors[0].code === 3010) ? { ...acc, alreadyIn: acc.alreadyIn + 1 } : { ...acc, errors: acc.errors + 1 }) :
      {...acc, inserted: acc.inserted + 1}
    , { errors: 0, alreadyIn: 0, inserted: 0 }
  ));
};

main();
