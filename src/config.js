export const {
  PRISMA_SECRET,
  DB_ENDPOINT,
  PROD_PRISMA_SECRET,
  PROD_DB_ENDPOINT,
} = process.env;
