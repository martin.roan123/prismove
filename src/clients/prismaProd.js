import 'dotenv/config';

import { PROD_PRISMA_SECRET, PROD_DB_ENDPOINT } from '../config';

import { Prisma } from '../generated/prisma-client';

const prismaProd = new Prisma({
  endpoint: PROD_DB_ENDPOINT,
  secret: PROD_PRISMA_SECRET,
});

Object.keys(prismaProd)
  .filter((key) => key.indexOf('update') === 0)
  .forEach((key) => { delete prismaProd[key]; });
Object.keys(prismaProd)
  .filter((key) => key.indexOf('upsert') === 0)
  .forEach((key) => { delete prismaProd[key]; });
Object.keys(prismaProd)
  .filter((key) => key.indexOf('create') === 0)
  .forEach((key) => { delete prismaProd[key]; });
Object.keys(prismaProd)
  .filter((key) => key.indexOf('delete') === 0)
  .forEach((key) => { delete prismaProd[key]; });

export default prismaProd;
