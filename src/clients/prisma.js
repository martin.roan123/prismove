import 'dotenv/config';

import { Prisma } from '../generated/prisma-client';
import { PRISMA_SECRET, DB_ENDPOINT } from '../config';

const prisma = new Prisma({
  endpoint: DB_ENDPOINT,
  secret: PRISMA_SECRET,
});

export default prisma;
